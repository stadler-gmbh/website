---
author: Amadeus Maximilian
blurb: >-
  Aktuell ist es in diesem CMS noch nicht möglich Binärdaten (Bilder, PDFs,
  etc.) hochzuladen
createdAt: '2021-10-22T11:40:23+02:00'
title: Umgang mit Binärdaten
---
Falls solche Dateien hochgeladen werden sollen, bitte Forestry verwenden, oder sie mir (Amadeus) zukommen lassen, damit ich sie direkt in die richtigen Ordner packen kann.

Diese Arten von Dateien können auch mit diesem CMS in Inhalte eingefügt werden – allerdings werden sie technisch bedingt in der Vorschau nicht sichtbar sein. Wenn die Änderungen dann veröffentlicht werden, sind die Dateien allerdings ganz normal verfügbar.

Bitte entschuldigt die Umstände.
