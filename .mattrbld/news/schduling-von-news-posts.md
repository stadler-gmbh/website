---
author: Amadeus Maximilian
blurb: >-
  Blog-Posts mit einem Veröffentlichungsdatum in der Zukunft werden auf der
  Website nicht angezeigt. Das kann dazu verwendet werden, Artikel zu einem
  bestimmten Zeitpunkt automatisch zu „veröffentlichen“. Diese Artikel dürfen
  aber nicht als „Draft“ markiert sein.
createdAt: '2022-12-02T10:32:57+01:00'
title: Scheduling von News-Posts
---
Dieses CMS arbeitet ohne einen Backend-Server und die resultierende Website wird nicht on-the-fly, sondern nur nach jeder veröffentlichten Änderung neu generiert. Dementsprechend gibt es keine Möglichkeit Inhalte automatisiert zu bestimmten Zeiten von **Draft** auf **Öffentlich** zu stellen.

Um dieses Problem zu umgehen, kann an in einem Blog-Artikel ein Veröffentlichungsdatum angeben, das in der Zukunft liegt und dann die Markierung als **Draft** aufheben und die Änderungen synchronisieren. Dadurch wird der Artikel für die Website generiert und hinterlegt, aber nicht angezeigt, bis das Veröffentlichungsdatum erreicht wird.

**Achtung**: der Artikel ist für Nutzer zwar nicht sichtbar, aber bereits im Quellcode und auf dem FTP-Server hinterlegt. Mit einer genauen URL ist er also theoretisch schon aufrufbar und von Suchmaschinen indexierbar. Dementsprechend sollten die Informationen in unsichtbaren Artikeln nicht als “geheim” betrachtet werden.
