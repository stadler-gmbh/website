---
author: Amadeus Maximilian
blurb: >-
  Hier finden sich alle fixen Bildgrößen an einem Ort. In den meisten Fällen
  sind diese auch in den Bild-Feldern der jeweiligen Content-Elemente
  hinterlegt, z.B. als "(560x680px)".
createdAt: '2022-06-16T09:48:49+02:00'
title: Bildgrößen
---
# Bildgrößen

## Allgemein

-   Bild für Social Media: **1200x630px**
    
-   Trenner-Blöcke (Kunden-Slider, CTA-Trenner, Icon-Trenner, Tab-Trenner): **1920x1080px**
    
-   Beziffertes-Raster: **2600px** breit, Höhe abhängig vom Content
    
-   Fullscreen-Slider: **2560x1440px**
    
-   Seitentitel: **2600x300px**
    
-   Download: **140px** hoch
    
-   Banner: **96x96px**
    

## Mitarbeiterbilder

-   Breite Darstellung: **1120x240px**
    
-   Normale Darstellung: **560x680px**
    

## Karriere

-   Offene-Stellen-Modul: **576x576px**
    

## Bildergalerie

-   Vorschaubild: **845px** breit
    
-   HD-Bild: **1280px** breit
    

*Die Höhe der Galeriebilder ist nicht vorgegeben, sollte aber zumindest bei den Vorschaubildern einheitlich sein, damit diese in einem schönen Raster angezeigt werden können.*

## Einzelbild

-   Desktop: **1152px** breit
    
-   Mobil: **480px** breit
    

## Spalten

-   Bei einer Spalte: **1152px** breit
    
-   Bei zwei Spalten: **544px** breit
    
-   Bei drei Spalten: **352px** breit
    

*Die Höhe der Spaltenbilder ist nicht vorgegeben, sollte aber bei mehreren Spalten einheitlich sein, damit der Text unterhalb des Bildes in allen Spalten auf der selben Höhe ist.*

## Blog

-   Hero-Bild: **744px** breit, mindestens **446px** hoch
