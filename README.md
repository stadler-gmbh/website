# Stadler GmbH Website

This is a complete refactor of the current [website](https://stadler-gmbh.de) in
order to streamline development and make content management easier.

### Setup

For now this is a Git repository with a Gridsome project within it. To
get started with coding, simply run `npm run dev` to start a development server
with live-reloading.

**Warning:** this project uses Git LFS for handling binary files. Please make
sure you have LFS installed and running to avoid any issues.

### Build

To generate a static version of this website for deployment on a web-server,
just run `npm run build` and move the resulting `/public` directory to your host
of choice. These steps are currently automated through the use of the GitLab CI.
