# Benötigte Komponenten

- [ ] Navigation
    - [X] Language Switcher
    - [X] Nav-Dropdown
    - [ ] Logos Unterbereiche
    - [X] Automatische Weiterleitung zu anderen Sprachen
- [X] Footer
    - [X] To-Top Button
    - [X] Responsive
- [X] Fullscreen Image Slider mit Inhalten
- ~~[ ] Cta-Buttons~~
- [X] Person-Highlight mit Bild, Overlay und Metadaten / Mitarbeiterkarte mit Unterschiedlichen Breiten
- [X] Buckets / 3-Spalten Layouts
- [X] Trenner mit abgedunkeltem Hintergrundbild und Support für Inhalte (Icons, Text, Buttons)
- [X] Icons
    - [X] Fontawesome
    - [X] et-line
- [X] "Scroll down for more" Indikator
- [X] Kunden-Slider
- [X] Akkordeon
- [X] Logo
- [X] Logo Spinner
- [X] Tab-UI mit großem Content
- [X] Verteiler-Karte mit Bild & Titel
- [X] Kontakt-Box
- [X] Breadcrumb
- [X] CTA Separator Block (Heading, Body, Image, CTA)
- [X] Icon Separator Block
- [X] "Deswegen Stadler Block" → Duplicated Content?
- [X] Header mit abgedunkeltem Hintergrundbild, Titel & Breadcrumb
- [X] "Offene Stellen Block" mit Bild & Akkordeon
- [X] Download Item Component
- [X] Download List Block
- [ ] Intersection-Observer für Visibiltäts-Basierende Animationen
- [X] Lightbox / Gallery

# Benötigte Templates / Layouts

* Start
  * Fullscreen Image Slider
  * Textblock und Überschrift
  * Mitarbeiterkarte groß Rüdiger
  * 3 Spalten / Buckets mit Logo, Überschrift und Liste
  * Trenner mit Icons und Text
  * Tabelle "Geschichte"

* Bereichs-Übersicht
  * Fullscreen Image Slider
  * 3 Spalten mit Titel
  * Mitarbeiterkarten
  * Tabs mit Überschrift
  * CTA Trenner
  * Textblock mit Überschrift
  * Verteilerkarten
  * CTA Trenner
  * 2x 3 Spalten mit 1x Titel ("Deswegen Stadler")
  * Kunden-Slider
  * 2x Kontakt Block

* Detailansicht "Referenz"
  * Header mit Titel und Breadcrumb
  * 2 Spalten Text
  * Roter Block mit Liste (2 Spalten)
  * Bilder-Raster & Lightbox
  * CTA Trenner

* FAQ
  * Header mit Titel und Breadcrumb
  * 2x Titel + Akkordeon

* Karriere
  * Header mit Titel und Breadcrumb
  * Titel + Text (1 Spalte)
  * "Offene Stellen Block"
  * CTA Trenner
  * "Offene Stellen Block"
  * CTA Trenner
  * Titel + Text (1 Spalte)
  * "Offene Stellen Block"
  * CTA Trenner
  * Titel + Text (1 Spalte)
  * CTA Trenner

* Downloads
  * Header mit Titel und Breadcrumb
  * "Download Block" (4 Spalten)

* Datenschutz
  * Header mit Titel und Breadcrumb
  * Text (1 Spalte)
  * Impressums-Akkordeon (?)

* Impressum
  * Impressums-Akkordeon
