# Analytics Options

## Google Analytics

### Pros:

- Free (as in Beer)
- Widespread
- Easy to implement
- Google Tag Console integration (?)

### Cons:

- Massive data collection
- Too much data to be useful
- User tracking
- Requires Cookie banner
- Datacenter not in EU (?)

## Plausible (https://plausible.io)

### Pros:

- Privacy Focussed
- Data is in Frankfurt
- Enough data is collected and presented clearly
- FOSS
- [Intuitive Dashboard](https://plausible.io/plausible.io)
- We own our data
- Google Search Console integration

### Cons:

- Not free as in beer (4$ to 8$ per month depending on pageviews)
- Data is not as comprehensive


## Simple Analytics (https://simpleanalytics.com)

### Pros:

- Privacy Focussed
- Servers in the Netherlands
- No cookie banners needed
- Shows essential data: page views, referrers, top pages, screen sizes
- Doesn’t collect personal data
- We own everything that’s collected

### Cons

- Not free as in beer (cheapest plan (100k page views) starts at 9$ a month paid yearly)
- Seems a little more expensive and a little less professional than Plausible


## Fathom (https://usefathom.com)

### Pros:

- Privacy focussed and GDPR + CCPA + PECR compliant
- No personal data collection
- No cookies
- Clearly lists a Privacy Officer

### Cons:

- A little pricey (14$ per 100k a month)

## Matomo (https://matomo.org)

### Pros:

- FOSS
- Collects a lot of data, probably comparable to GA
- Is already being used

### Cons:

- Hosted option is expensive! 29€ for 50k pageviews
- Shit tonnes of data collected, but not really presented in a neat overview
