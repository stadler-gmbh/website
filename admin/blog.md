# Blog Concept

## Features:

- Tags for the different departments
- Multi-Media support (including embedded Youtube videos)
- Support for authors based on the employees
- Timestamps (day being the smallest displayed unit)

## Questions

- Do we need tag pages or will the tags simply link to the landing pages of the departments? → Tags
- Do we cluster monthly or yearly in the backend? → Monthly
- How will multiple languages be handled? Will every post be translated or will we have different content per language? → Individual
- Will posts be modular and support a subset or all of the blocks? Or will they be predominantly text + metadata (markdown instead of JSON)? → MD

## Implementation

In order to be able to paginate, we will have to use a separate page template from the usual one.
This is due to a restriction in Gridsome that doesn’t allow component queries to contain parameters
which would be needed for pagination.

So to implement a Blog it would be useful to create a Blog-Template with some restrictions
on what blocks can be placed within its content area. I think it might make sense to be able
to include the following:

- Text
- CTA Separator
- Page Title (for the top)
- Blogroll (obviously)

We will also need a Blog-Post template that either supports a list of blocks or
renders the transpiled markdown.

It could be possible to put an additional statement in the server configuration
to pass some filtering options from the data file right into the context so there
could be a future distinction between the different departments.

## Markdown vs JSON

While a JSON implementation would be more flexible and allow for greater control,
it would also cause a lot of code duplication with the current setup that probably
would need to be refactored. It also might just not make sense to use such a sophisticated system
for something as simple as a blog.

Using a markdown plugin would allow us to use the large content-area of Forestry,
I’m not sure what would happen if it was a JSON file → maybe the markdown would be saved as a "content"-field?
That would require us to do our own markdown rendering, which would cause issues with g-link and g-image.
