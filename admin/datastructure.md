# Datenstruktur

Es gibt mehrere Möglichkeiten, die Daten zu strukturieren und dann in die
Templates einfließen zu lassen.

Jeweils ein Seiten-Template für jede Sprache anzulegen hat den Vorteil, dass
sich die Seiten je nach Sprache unterscheiden können, führt aber zu
Wiederholungen und bietet dadurch Raum für Fehlerquellen.

Die Seiten anhand eines einheitlichen Templates programmatisch zu erstellen
sollte möglich sein, könnte aber zu Problemen führen, wenn die Seiten nicht
exakt identisch aufgebaut sein sollen.

Da sich einige Text-Elemente doch recht Häufig wiederholen macht es eventuell
Sinn, diese an einem zentralen Ort abzulegen – dafür bedarf es allerdings
weiterer Analyse, um herauszufinden wie hoch die Wiederholungsrate effektiv ist
und ob dieser Ansatz den höheren Verwaltungsaufwand auf der Programmiererseite
besonders im Hinblick auf die Anbindung eines CMS-Frontends wert ist.

Den eigentlichen Content in JSON-Dateien abzulegen scheint in einem anderen
Projekt gut zu funktionieren und würde die Möglichkeit bieten, den Content für
alle Sprachen in einer einzigen Daten-Datei aufzubewahren, was auch zukünftige
Übersetzungsarbeiten vereinfachen könnte. Zudem ließen sich diese Daten-Dateien
eventuell maschinell generieren, was das obige Problem der sich wiederholdenden
Textpassagen lösen könnte.

Allerdings könnte die Änderung dieser Dateien dann im Konflikt mit der Anbindung
eines CMS stehen, wenn diese aus einem zentralen Datensatz heraus generiert
werden sollen.

## Anlegen der Daten in einer Multi-Sprachen Struktur

Es hat sich etabliert, Textpassagen (Strings) einen eindeutigen Identifikator
zu vergeben und diesen dann mit den unterschiedlichen Übersetzungen zu befüllen,
die über ihren Sprachcode identifiziert werden:

```json
{
  "title": {
    "de": "Über uns",
    "en": "About us"
  }
}
```

Sollte eine Übersetzung in einer Sprache nicht verfügbar sein, ließe sich diese
Texpassage über einen Filter für die Sprache ausblenden. Allerdings stellt sich
auch hier die Frage inwiefern die Struktur der Textpassagen die Seitenstruktur
nachbilden sollte: einerseits wäre es einfacher, zusammengehörige Passagen wie
Listen auch in der Struktur zusammenhängend aufzubauen, allerdings würde dies
die Datenstruktur fest an das zugehörige Template koppeln.
