---
name: Staatspreis für Auszubildende bei Stadler GmbH
date: 2020-08-10T00:00:00.000Z
authors:
  - rainer-nackenhorst
language: de
categories:
  - holding
heroImage:
  src: /content/uploads/news/S-Bihlmaier-Staatspreis_in_Zeitung-2_1-8-2020.jpg
teaser: >-
  Im Zuge der Abschlussprüfungen 2020, erreichte unsere Auszubildende Simone
  Bihlmaier eine TOP-5 Platzierung.
heroVideo: null
contact: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
> Im Zuge der Abschlussprüfungen 2020, erreichte unsere Auszubildende Simone Bihlmaier eine TOP-5 Platzierung.

Dass sich Zielstrebigkeit und Ausdauer in einer Berufsausbildung lohnt, beweist auf eindrucksvolle Weise unsere „AZUBINE“ Simone Bihlmaier. Ihre außergewöhnlichen Leistungen und Kenntnisse wurden zum Abschluss ihrer Ausbildung zur Kauffrau für Büromanagement, mit einem Staatspreis der Landesregierung Schwaben gewürdigt.

Als eine der fünf Besten in ganz Schwaben sind wir mächtig stolz auf unsere Simone und gratulieren auf diesem Weg nochmals. Nachdem ihre praktischen Leistungen in unserem Unternehmen genauso hervorragend waren wie die schulischen, freuen wir uns umso mehr über ihre  Entscheidung der STADLER-GmbH auf unbefristete Zeit im Büromanagement zur Verfügung zu stehen.

Herzlichst Rüdiger & Roland Stadler

_Bildquelle: Mindelheimer Zeitung_
