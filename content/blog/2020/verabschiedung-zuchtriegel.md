---
name: Eine Legende geht in Rente
date: 2020-12-04T00:00:00.000Z
authors:
  - rainer-nackenhorst
language: de
categories:
  - holding
heroImage:
  src: /content/uploads/news/Verabschiedung_I-Zuchtriegel.jpg
teaser: >-
  Nach 17 Jahren unermüdlichem Einsatz für die STADLER GmbH verabschiedet sich
  I. Zuchriegel in den Ruhestand.
heroVideo: null
contact: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
> Nach 17 Jahren unermüdlichem Einsatz für die STADLER GmbH verabschiedet sich I. Zuchriegel in den Ruhestand.

PFAFFENHAUSEN – Im Dezember 2020 war es soweit, unser Buchhaltungs- und Finanzmultitalent sowie die rechte Hand vom Chef, Irmgard Zuchtriegel, wurde für ihre langjährige Betriebszugehörigkeit vom  kompletten Verwaltungs-Team geehrt. Gleichzeitig verabschiedete sich Frau Zuchtriegel nach 17 Jahren in den wohlverdienten Ruhestand.

Auf Ihre größte Herausforderung während ihrer Betriebszugehörigkeit angesprochen, antwortet Sie: „Die betriebswirtschaftlich einwandfreie Umstellung der STADLER GmbH in drei Geschäftszweige STADLER ISOBAU, STADLER LUFTKLIMA & STADLER HOLDING  ab 2017, war it so oifach“.

Wir bedanken uns auf diesem Weg nochmals ganz herzlich, für die angenehme und fruchtbare Zusammenarbeit und wünschen dir liebe Irmgard, für deinen neuen Lebensabschnitt alles erdenklich Gute. Natürlich kannst Du uns jederzeit auf einen Kaffee besuchen.

Rüdiger & Roland Stadler
