---
name: SCHALLSCHUTZ IN DER LEBENSMITTEL-PRODUKTION
date: 2021-12-06T23:00:00.000Z
authors:
  - peter-huber
language: de
categories:
  - air-conditioning
  - facility-acoustics
heroImage:
  src: /content/uploads/stadler-lk_schallschutzwand-comp_1k-om.jpg
  alt:
    de: Zei Produktionmaschinen mit Schallschutzabdeckung
    en: Two production assemblies with a Sound Protection System
title: AA Schallschutz in der Lebensmittelproduktion
teaser: >-
  Individuell gestalteter Schallschutz führt zu einem stressreduzierten
  Arbeitsklima.
contact: peter-huber
heroVideo: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Im Sommer kam ein langjähriger Kunde auf uns zu und berichtete von seinen bisher unzureichenden Schallschutzmaßnahmen in seiner Käsefertigung. Unsere Spezialisten aus der Stadler Anlagenakustik analysierten die akustischen Gegebenheiten vor Ort. Dabei kamen unsere Akustische Kamera und Schallintensitäts-Sonde zum Einsatz. Die anschließende Auswertung und Berechnung ergab ein individuell ausgearbeitetes Schallschutzkonzept.

Neben dem Schallschutz bestand die Herausforderung dieses Projektes darin, eine hygienekonforme Konstruktion auf die knapp bemessenen Platzverhältnisse optimal abzustimmen. Darüber hinaus mußte eine robuste Bauweise gefunden um den Produktionsprozessen des Kunden gerecht zu werden.

Die Herstellung der Schallschutzmaßnahmen erfolgte in unsere Metallbauabteilung unter Verwendung hygienekonformer und akustischer Absorbersysteme. Nach dem Einbau der Verkapselungen konnte die prognostizierten Schallreduktion, von bis zu 4dB durch Schallmessungen bestätigt werden. Somit konnten wir unseren Kunden vollauf zufrieden stellen und den Mitarbeiter in der Käseproduktion ein entspannteres Arbeitsklima schaffen.
