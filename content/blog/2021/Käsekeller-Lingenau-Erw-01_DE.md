---
date: 2021-10-07T13:41:46.000Z
language: de
name: Käsekeller geht in die Verlängerung
teaser: >-
  Stadler GmbH erhält den Zuschlag für die Erweiterung der Käsekellerei im
  Bregenzer Wald.
authors:
  - ruediger-stadler
categories:
  - technical-construction
  - air-conditioning
heroImage:
  src: /content/uploads/kaesekeller-lingenau-top.jpg
  alt:
    de: Fabrikgebäude von oben
    en: ' Industrial plant from top'
    ru: ''
  title:
    de: ''
    en: ''
    ru: ''
heroVideo: null
contact: 'ruediger-stadler, florian-kirschenhofer'
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Der Bregenzerwälder Käsekeller in Lingenau platzte aus allen Nähten. Nachdem vor fünf Jahren im alten Alma-Gebäude in Hard ein provisorischer Reifekeller eingerichtet wurde, standen die Bagger für die Erweiterung in den Startlöchern und haben ganze Arbeit geleistet. Das bestehende Reifezentrum an der L 205 bekommt einen 40 Meter langen Zubau. Wie schon bei der Renovierung des Alma-Gebäudes in Hard erhielt den Zuschlag zur Klimatisierung der Erweiterung die Stadler Luftklima GmbH mit ihrem innovativen Reifesystem ESJET HX. Darüber hinaus wurde die Stadler Isobau mit dem kompletten Innenausbau inklusive Tür- & Toranlagen beauftragt.
