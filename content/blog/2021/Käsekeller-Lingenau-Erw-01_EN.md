---
date: 2021-10-07T13:51:18.000Z
language: en
name: Cheese cellar goes into extension
teaser: >-
  Stadler GmbH was awarded the contract to expand the cheese cellar in the
  Bregenz Forest.
authors:
  - ruediger-stadler
categories:
  - technical-construction
  - air-conditioning
heroImage:
  src: /content/uploads/kaesekeller-lingenau-top-1.jpg
  alt:
    de: ''
    en: Industrial plant from top view
    ru: ''
  title:
    de: ''
    en: ''
    ru: ''
heroVideo: null
contact: 'ruediger-stadler, florian-kirschenhofer'
___mb_schema: /.mattrbld/schemas/blog-post.json
---
The Bregenzerwald cheese cellar in Lingenau was bursting at the seams. After a provisional ripening cellar was set up in the old Alma building in Hard five years ago, the excavators for the expansion were in the starting blocks and did a great job. The existing ripening center on the L 205 is getting a 40-meter-long extension. As with the renovation of the Alma building in Hard, Stadler Luftklima GmbH with its innovative ESJET HX ripening system was awarded the contract to air-condition the extension. In addition, Stadler Isobau was commissioned with the complete interior work including door and gate systems.
