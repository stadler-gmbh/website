---
name: Ein Original geht in Rente
date: 2021-03-18T00:00:00.000Z
authors:
  - rainer-nackenhorst
language: de
categories:
  - technical-construction
heroImage:
  src: /content/uploads/news/Verabschiedung_G-Kwiatek.jpg
teaser: >-
  Nach 31 Jahren unermüdlichem Einsatz für die STADLER GmbH verabschiedet sich
  G. Kwiatek in den Ruhestand.
heroVideo: null
contact: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
> Nach 31 Jahren unermüdlichem Einsatz für die STADLER GmbH verabschiedet sich G. Kwiatek in den Ruhestand.

PFAFFENHAUSEN – „STADLER GmbH Kwiatek, Gruß Gott“ hieß es seit 1990, sobald jemand über die Zentralnummer angerufen hat. Nicht Wenige wollten über die Jahre hinweg wissen, welche Frau hinter dieser sympathischen Stimme steckt. Gabi Kwiatek entpuppte sich während Ihrer Zeit bei uns als vielseitiges Original mit unzähligen Talenten. Neben ihrer freundlichen und zuverlässigen Arbeitshaltung, behalten wir auch ihre expressive Art mit Hang zu Hard Rock bei Firmenfesten, freudvoll in Erinnerung.

Wir bedanken uns auf diesem Weg nochmals ganz herzlich, für die angenehme und fruchtbare Zusammenarbeit und wünschen dir liebe Gabi, für deinen neuen Lebensabschnitt alles erdenklich Gute.

Rüdiger & Roland Stadler
