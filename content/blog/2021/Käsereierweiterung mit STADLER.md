---
date: 2021-04-17T14:39:36.000Z
language: de
name: “Käsereierweiterung mit STADLER”
teaser: >-
  Mit neuer Produktionsanlage vergrößert sich die Bauernkäserei Leupolz um 800
  Quadratmeter.
authors:
  - rainer-nackenhorst
categories:
  - technical-construction
  - air-conditioning
heroImage:
  src: >-
    /content/uploads/stadler-gu-projekt_kaserei-leupold-spatenstich_2021-04-17.jpg
  alt:
    de: Spatenstich in Leupolz mit Rüdiger Stadler
    en: Ground-breaking ceremony in Leupolz with Rüdiger Stadler
    ru: ''
  title:
    de: ''
    en: ''
    ru: ''
heroVideo: null
contact: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
LEUPOLZ - Spatenstich für einen weiteren Meilenstein: Der 20 auf 40 Meter große und neun Millionen Euro teure Neubau der Bauernkäserei Leupolz hat seinen Anfang genommen. In ihm soll künftig die neue Käserei-Produktionsanlage Platz finden.

Wie schon 2007 für den Neubau des Salzbadgebäudes wurde für die Umsetzung des Neubaus wieder die STADLER ISOBAU GmbH als Generalunternehmer beauftragt. Entsprechend dem Wunsch der Genossenschaft berücksichtigt die STADLER ISOBAU GmbH möglichst viele regionale Firmen bei der Umsetzung des Projektes. Gebaut wird mit Beton und massiv, auch wegen des Schallschutzes für die Nachbarn.

Der Innenausbau erfolgt mit Glasbord Paneelen und einem säurefesten Fliesenboden. Ein Großteil der Installationen wird oberhalb der begehbaren Zwischendecke installiert. Die STADLER LUFTKLIMA GmbH erhielt ebenfalls einen Auftrag für die Planung und Realisierung eines wirtschaftlichen Lüftungs- & Hygienekonzeptes mit Wärmerückgewinnung. Mit einer maßgeschneiderten Teilklimaanlage werden optimalen Bedingungen zur Käseproduktion im Gebäude gewährleistet. Im Zuge einer ganzheitlichen Planung wurden außerdem Umweltschutzmaßnahmen, wie die Verlegung eines Bachlaufes, realisiert. Zusätzlich wird ein von außen erreichbarer Besuchergang in die Käserei integriert.

_Bildlegende von links: Oberbürgermeister Michael Lang, Leupolz Ortsvorsteher Anton Sieber, Rüdiger Stadler Geschäftsführer STADLER ISOBAU/LUFTKLIMA GmbH, Käserei Leupolz vertreten durch: Markus Stützenberger, Karl Schneider, Ulrich Graf und Michael Welte._
