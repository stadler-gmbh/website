---
date: 2021-07-08T13:59:59.000Z
language: en
name: Expansion of the cheese dairy with STADLER
teaser: >-
  With a new production facility, the Leupolz farm cheese dairy is enlarged by
  800 square meters.
authors:
  - rainer-nackenhorst
categories:
  - air-conditioning
  - technical-construction
heroImage:
  src: >-
    /content/uploads/stadler-gu-projekt_kaserei-leupold-spatenstich_2021-04-17.jpg
  alt:
    de: ''
    en: ''
    ru: ''
  title:
    de: ''
    en: ''
    ru: ''
heroVideo: null
contact: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
LEUPOLZ - Groundbreaking for another milestone: The new building of the Leupolz dairy farm, 20 by 40 meters in size and costing nine million euros, has begun. In the future, the new cheese dairy production facility will be housed in it.

As in 2007 for the new building of the salt bath, STADLER ISOBAU GmbH was commissioned as general contractor for the implementation of the new building. In accordance with the wishes of the cooperative, STADLER ISOBAU GmbH takes into account as many regional companies as possible when implementing the project. The building is made of concrete and massive, also because of the noise protection for the neighbors.

The interior work is done with glass shelf panels and an acid-proof tiled floor. Most of the installations are installed above the accessible false ceiling. STADLER LUFTKLIMA GmbH also received an order for the planning and implementation of an economical ventilation and hygiene concept with heat recovery. With a tailor-made partial air conditioning system, optimal conditions for cheese production in the building are guaranteed. In the course of holistic planning, environmental protection measures, such as the relocation of a stream, were also implemented. In addition, a visitor's corridor accessible from the outside will be integrated into the cheese dairy.

_Caption from left: Lord Mayor Michael Lang, Leupolz Mayor Anton Sieber, Rüdiger Stadler Managing Director STADLER ISOBAU / LUFTKLIMA GmbH, Leupolz cheese dairy represented by: Markus Stützenberger, Karl Schneider, Ulrich Graf and Michael Welte._
