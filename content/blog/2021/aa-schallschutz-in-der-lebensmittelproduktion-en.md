---
name: SOUND PROTECTION IN FOOD PRODUCTION
date: 2021-12-04T23:00:00.000Z
authors:
  - peter-huber
language: en
categories:
  - air-conditioning
  - facility-acoustics
heroImage:
  src: /content/uploads/stadler-lk_schallschutzwand-comp_1k.jpg
  alt:
    de: Zei Produktionmaschinen mit Schallschutzabdeckung
    en: Two production assemblies with a Sound Protection System
title: AA Schallschutz in der Lebensmittelproduktion (EN)
teaser: >-
  Individually designed noise protection leads to a stress-reduced working
  environment.
contact: peter-huber
heroVideo: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
In the summer, a long-standing customer approached us and reported about the inadequate soundproofing measures in his cheese production. Our specialists from Stadler System Acoustics analyzed the acoustic conditions on site. Our acoustic camera and sound intensity probe were used for this. The subsequent evaluation and calculation resulted in an individually developed noise protection concept.

In addition to soundproofing, the challenge of this project was to optimally match a hygiene-compliant construction to the tight space available. In addition, a robust construction had to be found in order to do justice to the customer's production processes.

The soundproofing measures were produced in our metal construction department using hygienic and acoustic absorber systems. After the encapsulation had been installed, the predicted sound reduction of up to 4dB was confirmed by sound measurements. We were therefore able to fully satisfy our customers and create a more relaxed working atmosphere for the cheese production staff.
