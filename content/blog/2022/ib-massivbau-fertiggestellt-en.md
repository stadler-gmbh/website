---
language: en
authors:
  - max-miethsam
categories:
  - technical-construction
date: '2022-04-28T14:01:23+02:00'
name: “Solid construction completed”
teaser: >-
  At the Martin Bauhofer Käserei in Bodnegg, the next phase of construction has
  been completed.
heroImage:
  src: /content/uploads/news/STADLER-IB_Massivbau-2.jpg
  alt:
    de: Rohbau eines Gebäudes mit einem Kran vor blauem Himmel
    en: Shell of a building with a crane against blue sky
    ru: Здание с краном на фоне голубого неба
  title: null
heroVideo: null
contact: max-miethsam
___mb_schema: /.mattrbld/schemas/blog-post.json
---
BODNEGG - We are proud of our contractor who successfully completed the next phase of construction in our GU project. The special challenge with this part of the trade was the professional connection of the floor slab to the existing building. The dimensions of the construction project are now slowly becoming apparent.
