---
language: en
authors:
  - max-miethsam
categories:
  - air-conditioning
  - technical-construction
date: '2022-10-08T08:48:03+02:00'
name: “Cheese shopping experience and production”
teaser: >-
  The traditional Bauhofer family cheese dairy officially opened a new cheese
  shop at its production site in Kofeld-Bodnegg on October 8th.
heroImage:
  src: /content/uploads/news/2022/stadler-bauhofer-ladeneroeffnung__01_744w.jpg
  alt:
    de: >-
      Eine Gruppe Menschen steht vor einem modernen Gebäude, dessen Eingang mit
      blauen Luftballons verziert ist.
    en: >-
      A group of people is standing in front of a modern building, the entrance
      of which is decorated with blue balloons.
    ru: >-
      Группа людей стоит перед современным зданием, вход в которое украшен
      голубыми воздушными шарами.
  title: null
heroVideo: null
contact: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
BODNEGG - Our STADLER ISOBAU GmbH division was commissioned as general contractor for the turnkey constructional production expansion and the newly designed sales and training facilities. The scope of services includes the complete building complex including the interior ripening rooms and the facade design. The individually tailored ventilation system from our sister company STADLER LUFTKLIMA GmbH ensures the best climatic conditions in the production area and the ripening cellar.

We would like to take this opportunity to thank you again for the trust you have placed in STADLER ISOBAU & LUFTKLIMA and for the constructive cooperation.
