---
language: de
authors:
  - peter-huber
categories:
  - facility-acoustics
  - metal-engineering
  - technical-construction
date: '2022-12-02T11:11:00+01:00'
name: Lüftungsmodul für eine CO2-freie Zukunft auf dem Weg zum Kunden!
teaser: >-
  Es ist vollbracht. Wir sind richtig stolz unser erstes Lüftungsmodul
  ausliefern zu können.
heroImage:
  src: /content/uploads/news/2022/STADLER-LK-AA_LKid-Abtransport_744w.jpeg
  alt:
    de: >-
      Ein LKW transportiert ein verpacktes Lüftungsmodul ab. Auf der Verpackung
      ist ein Banner mit der Aufschrift "Partner für eine Emissionsfreie Zukunft"
      zu sehen, das die Stadler Luftklima bewirbt.
    en: >-
      A truck transports away a packaged ventilation module. A banner advertising
      Stadler Air Contiditioning with the words "Partner for an emission-free future"
      can be seen on the packaging.
    ru: >-
      Грузовик увозит упакованный вентиляционный модуль. На упаковке размещен
      баннер с надписью "Партнер для безэмиссионного будущего", рекламирующий
      Stadler Luftklima.
  title: null
heroVideo: null
contact: peter-huber
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Dank der vielfältigen STADLER Kompetenzfelder, waren wir in der Lage alle Aspekte des komplexen Anforderungsprofils unseres Kunden zu erfüllen. Beginnend mit der statischen, strömungstechnischen, akustischen und klimatechnischen Auslegung bis hin zur Fertigung des Stahlbaus haben sich unsere Spezialisten richtig ins Zeug gelegt. Die parallellaufende Steuerungsprogrammierung, der abschließende Zusammenbau der Komponenten sowie die Inbetriebnahme mit Testlauf und der Abnahmemessungen waren weitere Projektschritte die wir erfolgreich realisiert haben.

Sind Sie auf der Suche nach einem zuverlässigen Lieferanten der themenübergreifend Lüftungssysteme konzipiert und realisiert? Dann kontaktieren Sie uns.
