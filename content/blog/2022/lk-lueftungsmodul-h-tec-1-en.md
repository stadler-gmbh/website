---
language: en
authors:
  - peter-huber
categories:
  - facility-acoustics
  - metal-engineering
  - technical-construction
date: '2022-12-02T11:11:00+01:00'
name: Ventilation module for a CO2-free future on the way to the customer!
teaser: >-
  It is finished. We are really proud to be able to deliver our first
  ventilation module.
heroImage:
  src: /content/uploads/news/2022/STADLER-LK-AA_LKid-Abtransport_744w.jpeg
  alt:
    de: >-
      Ein LKW transportiert ein verpacktes Lüftungsmodul ab. Auf der Verpackung
      ist ein Banner mit der Aufschrift "Partner für eine Emissionsfreie Zukunft"
      zu sehen, das die Stadler Luftklima bewirbt.
    en: >-
      A truck transports away a packaged ventilation module. A banner advertising
      Stadler Air Contiditioning with the words "Partner for an emission-free future"
      can be seen on the packaging.
    ru: >-
      Грузовик увозит упакованный вентиляционный модуль. На упаковке размещен
      баннер с надписью "Партнер для безэмиссионного будущего", рекламирующий
      Stadler Luftklima.
  title: null
heroVideo: null
contact: peter-huber
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Thanks to the diverse STADLER fields of competence, we were able to fulfill all aspects of our customer's complex requirement profile. Starting with the static, aerodynamic, acoustic and climate-related design through to the manufacture of the steel structure, our specialists have really worked hard. The parallel control programming, the final assembly of the components as well as the commissioning with a test run and the acceptance measurements were further project steps that we successfully implemented.

Are you looking for a reliable supplier who designs and implements interdisciplinary ventilation systems? Then contact us.
