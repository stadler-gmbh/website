---
language: de
authors:
  - max-miethsam
categories:
  - technical-construction
date: '2022-04-28T14:01:23+02:00'
name: “Massivbau fertiggestellt”
teaser: >-
  Bei der Fa. Martin Bauhofer Käserei in Bodnegg ist der nächste Bauabschnitt
  abgeschlossen.
heroImage:
  src: /content/uploads/news/STADLER-IB_Massivbau-2.jpg
  alt:
    de: Rohbau eines Gebäudes mit einem Kran vor blauem Himmel
    en: Shell of a building with a crane against blue sky
    ru: Здание с краном на фоне голубого неба
  title: null
heroVideo: null
contact: max-miethsam
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Wir sind stolz auf unseren Bauunternehmer, der erfolgreich den nächsten Bauabschnitt in unserem GU-Projekt fertiggestellt hat. Die besondere Herausforderung bei diesem Teilgewerk war der fachmännische Anschluss der Bodenplatte an den Bestand. Nun werden langsam die Dimension des Bauvorhabens ersichtlich.
