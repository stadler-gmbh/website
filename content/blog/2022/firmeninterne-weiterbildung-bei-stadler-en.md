---
language: en
authors:
  - matthias-brenner
categories:
  - air-conditioning
  - technical-construction
date: '2022-09-23T09:07:57+02:00'
name: In-House Training at STADLER
teaser: The Stadler formula for our skilled workers’ success.
heroImage:
  src: /content/uploads/news/2022/stadler-ib_bg-bau-schulung_744w.jpg
  alt:
    de: >-
      Eine Menschenmenge sitzt auf Bierbänken in einer Lagerhalle und hört einem
      Vortragenden zu, der etwas auf einer portablen Leinwand präsentiert.
    en: >-
      A crowd sits on beer benches in a warehouse listening to a speaker present
      something on a portable screen.
    ru: >-
      Толпа сидит на пивных скамейках на складе и слушает оратора,
      представляющего что-то на портативном экране.
  title: null
heroVideo: null
contact: null
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Pfaffenhausen - At Stadler, every employee has the opportunity to work individually on their personal and professional development - we call this our “Stadler formula”. This consists of three building blocks:

First things first: Of course, the question of personal interests and inclinations always plays a role when choosing the right further education. After all, the further training should be fun and help to develop further – both personally and professionally.

Secondly, it is important that the goals to be achieved with the training are clear. For example, an employee might want to acquire new knowledge in a certain area. Or is he looking for a way to advance professionally or privately? It is therefore helpful to think carefully about what you want to achieve in advance.

And thirdly, it should be taken into account which phase of a career or training the employee is currently in. Depending on whether someone is at the beginning of their professional life or has already gained some experience, this naturally has different effects on the appropriate type of further training.

This is how we do justice to every employee and guarantee the highest quality standards!
