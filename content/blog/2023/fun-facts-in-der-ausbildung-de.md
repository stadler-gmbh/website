---
language: de
authors:
  - simone-bihlmaier
  - cagla-kiyak
categories:
  - holding
  - air-conditioning
  - metal-engineering
  - technical-construction
date: '2023-03-09T10:30:00+01:00'
name: Fun + Facts in der Ausbildung
teaser: Mit den STADLER-Mentoren macht die Ausbildung sogar Spaß
heroImage:
  src: /content/uploads/news/2023/Azubi-Mentors_in_Lounge.jpg
  alt:
    de: >-
      Zwei junge Mitarbeiterinnen erklären die Struktur der Stadler Holding
      anhand eines Schaubilds auf einem großen Monitor. Vier weitere
      Mitarbeitende sitzen an einem Tisch und hören aufmerksam zu. 
    ru: >-
      Две молодые сотрудницы объясняют структуру холдинга Stadler с помощью
      диаграммы на большом мониторе. Четверо других сотрудников сидят за столом
      и внимательно слушают. 
    en: >-
      Two young female employees explain the structure of Stadler Holding using
      a diagram on a large monitor. Four other employees sit at a table and
      listen attentively.
  title: null
heroVideo: null
contact: cagla-kiyak
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Seit Herbst letzten Jahres stehen wir für unsere AZUBIS mit Herz und Seele bereit. Vom ersten Tag bis zum Prüfungsabschluss sind wir an eurer Seite. Wir unterstützen euch bei jeglichen Anliegen wie schulische Herausforderungen oder Arbeitsabläufen. Wenn ihr jemanden zum Reden braucht, haben wir immer ein offenes Ohr für euch.

Um mehr Spaß in eure Ausbildung zu bringen, unternehmen wir gemeinsam vielfältige Ausflüge. Wir freuen uns auf eure Vorschläge und eine unvergessliche Zeit zusammen.

![](/content/uploads/news/2023/Azubi-Mentors_HandOff.jpg)
