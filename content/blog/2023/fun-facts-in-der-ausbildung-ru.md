---
language: ru
authors:
  - simone-bihlmaier
  - cagla-kiyak
categories:
  - holding
  - air-conditioning
  - metal-engineering
  - technical-construction
date: '2023-03-09T10:30:00+01:00'
name: Веселье + факты на тренировке
teaser: Тренироваться с наставниками STADLER даже весело
heroImage:
  src: /content/uploads/news/2023/Azubi-Mentors_in_Lounge.jpg
  alt:
    de: >-
      Zwei junge Mitarbeiterinnen erklären die Struktur der Stadler Holding
      anhand eines Schaubilds auf einem großen Monitor. Vier weitere
      Mitarbeitende sitzen an einem Tisch und hören aufmerksam zu. 
    ru: >-
      Две молодые сотрудницы объясняют структуру холдинга Stadler с помощью
      диаграммы на большом мониторе. Четверо других сотрудников сидят за столом
      и внимательно слушают. 
    en: >-
      Two young female employees explain the structure of Stadler Holding using
      a diagram on a large monitor. Four other employees sit at a table and
      listen attentively.
  title: null
heroVideo: null
contact: cagla-kiyak
___mb_schema: /.mattrbld/schemas/blog-post.json
---
С осени прошлого года мы всей душой и сердцем готовились к нашему AZUBIS. С первого дня до различных экзаменов мы рядом с вами. Мы поддерживаем вас с любыми проблемами, такими как школьные проблемы или новые задачи. Если вам нужно с кем-то поговорить, мы всегда открыты для вас. Чтобы сделать ваше обучение более увлекательным, мы вместе отправляемся в несколько поездок. Мы с нетерпением ждем ваших предложений и незабываемого времени с вами.

С осени прошлого года мы всей душой и сердцем готовились к нашему AZUBIS. С первого дня и до конца экзамена мы рядом с вами. Мы поддерживаем вас с любыми проблемами, такими как школьные проблемы или рабочие процессы. Если вам нужно с кем-то поговорить, мы всегда открыты для вас.

Чтобы сделать ваше обучение более увлекательным, мы вместе отправляемся на разнообразные экскурсии. Мы с нетерпением ждем ваших предложений и незабываемого времени вместе.

![Сотрудник дарит стажеру красный джемпер в комнате отдыха персонала.](/content/uploads/news/2023/Azubi-Mentors_HandOff.jpg)
