---
language: en
authors:
  - monika-fischer
categories:
  - holding
  - air-conditioning
  - technical-construction
  - metal-engineering
date: '2023-01-20T16:15:00+01:00'
name: German course at STADLER
teaser: ' Hallo, Ahoj, Përshëndetje, Olá, Здравейте, bună ziua, zdravo, नमस्ते, hello.'
heroImage:
  src: /content/uploads/news/2023/STADLER-Deutschkurs__1.JPG
  alt:
    de: >-
      Gruppe von Mitarbeitenden an einem langen Tisch beim Deutsch-lernen. Der
      Lehrer steht etwas abseits und erklärt etwas. Im Vordergrund ist ein
      Kinderwagen zu sehen.
    en: >-
      Group of employees at a long table learning German. The teacher stands a
      bit apart and explains something. A stroller can be seen in the
      foreground.
    ru: >-
      Группа сотрудников за длинным столом изучает немецкий язык. Преподаватель
      стоит чуть поодаль и что-то объясняет. На переднем плане видна детская
      коляска.
  title: null
heroVideo: null
contact: monika-fischer
___mb_schema: /.mattrbld/schemas/blog-post.json
---
We now speak sixteen different languages. In other words, we’re a big international family with occasional barriers to communication. As with all the challenges we encounter, we found a solution this time too: the Stadler German course after work. Very flexible, without coercion or pressure and with a lot of fun.

![A different shot of the employee lounge with a big clock visible on the wall. The employees are still studying, the teacher is checking their work.](/content/uploads/news/2023/STADLER-Deutschkurs__2.JPG)

After a retired German teacher agreed to support us in the project and the learning material was found, we could start. Thanks to the impressive performance of our teacher, the employees are there with a lot of diligence and zeal. The management and all colleagues are really proud and wish the participants every success. We believe in you!

Of course, the offer also applies to all newcomers.
