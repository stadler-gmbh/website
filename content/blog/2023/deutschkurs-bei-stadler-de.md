---
language: de
authors:
  - monika-fischer
categories:
  - holding
  - air-conditioning
  - technical-construction
  - metal-engineering
date: '2023-01-20T16:15:00+01:00'
name: Deutschkurs bei STADLER
teaser: ' Hallo, Ahoj, Përshëndetje, Olá, Здравейте, bună ziua, zdravo, नमस्ते, hello.'
heroImage:
  src: /content/uploads/news/2023/STADLER-Deutschkurs__1.JPG
  alt:
    de: >-
      Gruppe von Mitarbeitenden an einem langen Tisch beim Deutsch-lernen. Der
      Lehrer steht etwas abseits und erklärt etwas. Im Vordergrund ist ein
      Kinderwagen zu sehen.
    en: >-
      Group of employees at a long table learning German. The teacher stands a
      bit apart and explains something. A stroller can be seen in the
      foreground.
    ru: >-
      Группа сотрудников за длинным столом изучает немецкий язык. Преподаватель
      стоит чуть поодаль и что-то объясняет. На переднем плане видна детская
      коляска.
  title: null
heroVideo: null
contact: monika-fischer
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Mittlerweile wird bei uns in sechzehn verschiedenen Sprachen gesprochen. Mit anderen Worten, wir sind eine große internationale Familie mit gelegentlichen Kommunikationsschranken. Wie für alle Herausforderungen denen wir begegnen, fanden wir auch diesmal eine Lösung: Der Stadler-Deutschkurs nach dem Feierabend. Ganz flexibel, ohne Zwang oder Druck und mit viel Spaß.

![](/content/uploads/news/2023/STADLER-Deutschkurs__2.JPG)

Nachdem ein pensionierter Deutschlehrer sich bereit erklärt hat, uns bei dem Vorhaben zu unterstützen und das Lernmaterial gefunden wurde, konnte es losgehen. Dank der beeindruckenden Leistung unseres Lehrers sind die Mitarbeiter:innen mit viel Fleiß und Eifer dabei. Die Geschäftsführung und alle Kolleg:innen sind richtig stolz und wünschen den Teilnehmer:innen viel Erfolg.

Wir glauben an Euch!

Natürlich gilt das Angebot auch für alle Neuzugänge.
