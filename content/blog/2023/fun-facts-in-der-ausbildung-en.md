---
language: en
authors:
  - simone-bihlmaier
  - cagla-kiyak
categories:
  - holding
  - air-conditioning
  - metal-engineering
  - technical-construction
date: '2023-03-09T10:30:00+01:00'
name: Fun + Facts in your apprenticeship
teaser: 'With the STADLER  mentors, your apprenticeship will be fun!'
heroImage:
  src: /content/uploads/news/2023/Azubi-Mentors_in_Lounge.jpg
  alt:
    de: >-
      Zwei junge Mitarbeiterinnen erklären die Struktur der Stadler Holding
      anhand eines Schaubilds auf einem großen Monitor. Vier weitere
      Mitarbeitende sitzen an einem Tisch und hören aufmerksam zu. 
    ru: >-
      Две молодые сотрудницы объясняют структуру холдинга Stadler с помощью
      диаграммы на большом мониторе. Четверо других сотрудников сидят за столом
      и внимательно слушают. 
    en: >-
      Two young female employees explain the structure of Stadler Holding using
      a diagram on a large monitor. Four other employees sit at a table and
      listen attentively.
  title: null
heroVideo: null
contact: cagla-kiyak
___mb_schema: /.mattrbld/schemas/blog-post.json
---
Since last fall, we’ve been available with heart and soul for our new apprentices. We’re at your side from your first day to your final exam. We’ll support you with any concerns, be it challenges in school or during work processes. If you ever need someone to talk, we will listen.

To bring more fun into your training, we’ll go on various excursions together. We’re looking forward to your suggestions and an unforgettable time together.

![](/content/uploads/news/2023/Azubi-Mentors_HandOff.jpg)
