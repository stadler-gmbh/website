import metadata from '@/data/metadata.json';

export default function generateCanonicalLinks(lang, urls, path) {
  if (urls) {
    const urlForLang = JSON.parse(urls)[lang]; // allLanguages is a JSON String
    if (urlForLang) return [{ rel: 'canonical', href: urlForLang }];
  }
  return [{ rel: 'canonical', href: `${metadata.siteUrl}${path}` }];
}
