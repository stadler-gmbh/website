import metadata from '@/data/metadata.json';

function generateMetaTags(seo = {}, defaultTitle, lang = 'de') {
  const meta = [];

  if (defaultTitle) {
    meta.push({
      key: 'og:title',
      property: 'og:title',
      content: `${defaultTitle} – ${metadata.siteName}`,
    });
  }

  if (seo.description) {
    meta.push({
      key: 'og:description',
      property: 'og:description',
      content: seo.description,
    });
    meta.push({
      key: 'description',
      name: 'description',
      content: seo.description,
    });
  } else {
    meta.push({
      key: 'og:description',
      property: 'og:description',
      content: metadata.siteDescription[lang],
    });
    meta.push({
      key: 'description',
      name: 'description',
      content: metadata.siteDescription[lang],
    });
  }

  if (seo.image && seo.image.src) {
    meta.push({
      key: 'og:image',
      property: 'og:image',
      content: `${metadata.siteUrl}${seo.image.src.src}`, // image.src is an object containing metadata, the actual src is in src.src
    });

    if (seo.image.alt) {
      meta.push({
        key: 'twitter:image:alt',
        name: 'twitter:image:alt',
        content: seo.image.alt,
      });
    }
  }

  if (seo.cardStyle) {
    meta.push({
      key: 'twitter:card',
      name: 'twitter:card',
      content: seo.cardStyle, // can only be 'summary' or 'summary_large_image'
    });
  }

  if (seo.keywords && seo.keywords.length > 0 && seo.keywords.filter((keyword) => keyword && typeof keyword === 'string').length > 0) {
    meta.push({
      key: 'keywords',
      name: 'keywords',
      content: seo.keywords.join(', '),
    });
  } else if (metadata.siteKeywords[lang] && metadata.siteKeywords[lang].length > 0) {
    meta.push({
      key: 'keywords',
      name: 'keywords',
      content: metadata.siteKeywords[lang].join(', '),
    });
  }

  meta.push({
    key: 'og:url',
    property: 'og:url',
    content: `${metadata.siteUrl}${seo.url}`,
  });

  return meta;
}

export default generateMetaTags;
