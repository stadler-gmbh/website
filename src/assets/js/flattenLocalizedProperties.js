import { fallbackLanguage } from '@/data/supportedLanguages.json';

// Deeply flattens values that have a "localizedString" property
// So { name: { localizedString: "Foo" } } turns into { name: "Foo" }
export default function flattenLocalizedProperties(obj) {
  if (typeof obj !== 'object' || obj === null || typeof obj === 'undefined') return obj;

  const copy = { ...obj };

  if (Object.prototype.hasOwnProperty.call(copy, 'localizedString')) return copy.localizedString;

  Object.keys(copy).forEach((key) => {
    if (Array.isArray(copy[key])) copy[key] = copy[key].map((el) => flattenLocalizedProperties(el));
    else if (typeof copy[key] === 'object') copy[key] = flattenLocalizedProperties(copy[key]);
  });

  return copy;
}

export function flattenLanguageProperties(obj, lang) {
  if (!lang || typeof obj !== 'object' || obj === null || typeof obj === 'undefined') return obj;

  const copy = { ...obj };

  if (Object.prototype.hasOwnProperty.call(copy, lang)) return copy[lang] || copy[fallbackLanguage];

  Object.keys(copy).forEach((key) => {
    if (Array.isArray(copy[key])) copy[key] = copy[key].map((el) => flattenLanguageProperties(el, lang));
    else if (typeof copy[key] === 'object') copy[key] = flattenLanguageProperties(copy[key], lang);
  });

  return copy;
}

export function flattenLanguagePropertyString(obj, lang) {
  if (!lang || typeof obj !== 'object' || obj === null || typeof obj === 'undefined') return obj;

  const copy = { ...obj };

  if (Object.prototype.hasOwnProperty.call(copy, 'allLanguages')) return JSON.parse(copy.allLanguages)[lang] || JSON.parse(copy.allLanguages)[fallbackLanguage];

  Object.keys(copy).forEach((key) => {
    if (Array.isArray(copy[key])) copy[key] = copy[key].map((el) => flattenLanguagePropertyString(el, lang));
    else if (typeof copy[key] === 'object') copy[key] = flattenLanguagePropertyString(copy[key], lang);
  });

  return copy;
}
