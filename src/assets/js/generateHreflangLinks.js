import metadata from '@/data/metadata.json';

export default function generateHreflangLinks(rawUrls = '{}', rawCanonicalUrls = '{}') {
  const links = [];
  const urls = JSON.parse(rawUrls);
  const canonicalUrls = JSON.parse(rawCanonicalUrls) || {};

  Object.keys(urls).forEach((lang) => {
    const url = canonicalUrls[lang] || urls[lang]; // it could be that this is problematic, since articles seem to suggest that sites with canonical urls that don’t reference themselves shouldn’t have hreflang tags, we’ll need to observe. It might be okay, since if there is a canonical url the hreflang points to that as well
    links.push({ rel: 'alternate', hreflang: lang, href: url.startsWith('http') ? url : `${metadata.siteUrl}${url}` });
  });

  const defaultUrl = canonicalUrls.en || urls.en || canonicalUrls.de || urls.de;

  if (defaultUrl) links.push({ rel: 'alternate', hreflang: 'x-default', href: defaultUrl.startsWith('http') ? defaultUrl : `${metadata.siteUrl}${defaultUrl}` });

  return links;
}
