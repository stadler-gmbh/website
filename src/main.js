// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import NProgress from 'nprogress';

import 'normalize.css';
import '@fortawesome/fontawesome-svg-core/styles.css';

import '@fontsource/open-sans/300.css';
import '@fontsource/open-sans/400.css';
import '@fontsource/open-sans/600.css';
import '@fontsource/dosis/400.css';
import '@fontsource/dosis/700.css';

import '@/assets/styles/nprogress.styl';
import '@/assets/styles/base.styl';

import { fallbackLanguage, supportedLanguages } from '@/data/supportedLanguages.json';

// Import global components
import DefaultLayout from '@/layouts/Default.vue';

export default (Vue, { appOptions, router, head, isClient }) => { // eslint-disable-line no-unused-vars, object-curly-newline
  // Set the default language to German
  head.htmlAttrs = { lang: 'de' }; // eslint-disable-line no-param-reassign

  // Add a custom noscript style tag, fixes g-image and missing noscript support when JS is disabled
  head.noscript = [ // eslint-disable-line no-param-reassign
    { innerHTML: '<style>.g-image--loading{ display: none !important; }.nojs { display: block !important; }</style>' },
  ];

  // Needed for enabling plausible 404 and other custom events detection
  head.script.push({
    innerHTML: 'window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }',
  });

  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout);

  // import base components for convenience
  const requireComponent = require.context('./components', false, /Stadler[A-Z]\w+\.(vue|js)$/);

  requireComponent.keys().forEach((fileName) => {
    const componentConfig = requireComponent(fileName);
    const componentName = fileName.split('/').pop().replace(/\.\w+$/, '');
    Vue.component(componentName, componentConfig.default || componentConfig);
  });

  // Set up nprogress
  if (process.isClient) { // NProgress requires document to be defined
    NProgress.configure({ showSpinner: false });

    router.beforeEach((to, from, next) => {
      if (to.path !== from.path) NProgress.start();
      next();
    });

    router.afterEach(() => {
      NProgress.done();
    });

    // handle clicking on an anchor with the same target as the current hash
    window.addEventListener('click', (e) => {
      if (e.target.tagName.toLowerCase() !== 'a' || !window.location.hash || !e.target.getAttribute) return;

      const href = e.target.getAttribute('href');
      if (href === window.location.hash) {
        const el = document.getElementById(window.location.hash.slice(1));
        if (el) el.scrollIntoView();
      }
    }, true);
  }

  // Set up language field
  if (process.isClient) { // language detection needs navigator
    const userLanguage = navigator.language || fallbackLanguage;
    const [lang] = userLanguage.split('-');
    if (supportedLanguages.includes(lang)) appOptions.data.lang = lang; // eslint-disable-line no-param-reassign
    else appOptions.data.lang = fallbackLanguage; // eslint-disable-line no-param-reassign
  } else {
    // default language is German
    appOptions.data.lang = 'de'; // eslint-disable-line no-param-reassign
  }

  router.options.scrollBehavior = function customScrollBehavior(to, from, savedPosition) { // eslint-disable-line no-param-reassign
    let position = {};
    if (savedPosition) position = savedPosition;
    else if (to.hash) {
      if (to.path === from.path) return { selector: to.hash, offset: { x: 0, y: 128 } };
      position = { selector: to.hash, offset: { x: 0, y: 128 } };
    } else position = { x: 0, y: 0 };
    return new Promise((resolve) => {
      this.app.$root.$once('triggerScroll', () => {
        resolve(position);
      });
    });
  };
};
