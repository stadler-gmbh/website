// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const metadata = require('./src/data/metadata.json');

module.exports = {
  images: {
    defaultBlur: 10,
  },
  metadata: {
    siteKeywords: metadata.siteKeywords.de,
  },
  outputDir: 'public',
  siteDescription: metadata.siteDescription.de,
  siteName: metadata.siteName,
  siteUrl: metadata.siteUrl,
  titleTemplate: 'STADLER—%s',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'PageNode',
        path: './content/pages/**/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'BlogNode',
        path: './content/blog/**/*.md',
        remark: {
          slug: false,
        },
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Employee',
        path: './content/employees/**/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'OpenPosition',
        path: './content/open-positions/**/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'DownloadItem',
        path: './content/downloads/**/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Banner',
        path: './content/banners/**/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Category',
        path: './content/categories/**/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        config: {
          '/**': {
            lastmod: '2022-11-04',
          },
        },
        exclude: ['/en/', '/en/**', '/ru/', '/ru/**'],
        output: '/sitemap_de.xml',
      },
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        config: {
          '/**': {
            lastmod: '2022-11-04',
          },
        },
        include: ['/en/', '/en/**'],
        output: '/sitemap_en.xml',
      },
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        config: {
          '/**': {
            lastmod: '2022-11-04',
          },
        },
        include: ['/ru/', '/ru/**'],
        output: '/sitemap_ru.xml',
      },
    },
    {
      use: 'gridsome-plugin-plausible-analytics',
      options: {
        dataDomain: 'stadler-gmbh.de',
        outboundLinkTracking: true,
        excludePages: [
          '/___preview',
        ],
      },
    },
  ],
  templates: {
    BlogNode: [{
      path: '/news/:language/:year/:month/:name',
    }],
  },
};
