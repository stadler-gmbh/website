// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const slugify = require('@sindresorhus/slugify');
const { fallbackLanguage, supportedLanguages } = require('./src/data/supportedLanguages.json');
const { category: categoryTranslated, openPositionSlug } = require('./src/data/commonStrings.json');

function typesForAllLangs(type) {
  return supportedLanguages.map((lang) => `${lang}: ${type}`).join(',\n        ');
}

module.exports = (api) => {
  api.loadSource(({ addSchemaResolvers, addSchemaTypes, getCollection, schema }) => { // eslint-disable-line object-curly-newline
    addSchemaTypes(`
      # Type for Multi-Language Strings
      type LanguageStrings {
        ${typesForAllLangs('String')}
      }

      # Type for Multi-Language String Arrays
      type LanguageStringArray {
        ${typesForAllLangs('[String]')}
      }

      # Type for images with metadata
      # "Image" is already defined by Gridsome
      type TaggedImage {
        src: Image!
        alt: LanguageStrings
        title: LanguageStrings
      }

      # Type for Calls To Action
      type Cta {
        label: LanguageStrings
        href: LanguageStrings
        title: LanguageStrings
      }

      # Employee Type (imported from content/employees)
      type Employee implements Node {
        email: String
        greeting: LanguageStrings
        id: ID!
        image: TaggedImage
        message: LanguageStrings
        name: LanguageStrings
        role: LanguageStrings
        hideInMosaic: Boolean
      }

      # Breadcrumb Segment Type for generating slugified breadcrumbs and matching URLs
      type BreadcrumbSegment {
        label: LanguageStrings
        href: LanguageStrings
        hash: String
      }

      # Open Position Type
      type OpenPosition implements Node {
        id: ID!
        name: LanguageStrings
        body: LanguageStrings
        section: String
        url: LanguageStrings
      }

      # Download Item Type
      type DownloadItem implements Node {
        id: ID!
        languages: [String],
        name: LanguageStrings
        date: Date
        description: LanguageStrings
        file: File
        thumbnail: TaggedImage
        categories: [Category] @reference(by: "id")
      }

      # Banner Type
      type Banner implements Node {
        id: ID!
        image: TaggedImage
        body: LanguageStrings # may contain inline HTML
        expires: Date
      }

      # Type for Blog categories
      type Category implements Node {
        id: ID!
        url: LanguageStrings
        label: LanguageStrings
      }

      # Block Types
      # Interface is defined below and looks like this:
      # interface Block {
      #   template: String!
      #   id: String
      #   shortLinkLabel: LanguageStrings
      # }

      # Generic text block with a heading and up to three columns
      # LanguageStrings in Columns can include HTML
      type TextBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        columns: [LanguageStrings]
        highlighted: Boolean
      }

      # Longform single-column text block
      type LongformTextBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        body: LanguageStrings # can contain HTML
        useHeadingSeparators: Boolean
      }

      # Columns Block (like a Text-Block but with heading and image support)
      type Column {
        heading: LanguageStrings
        image: TaggedImage
        body: LanguageStrings # may contain HTML
      }

      type ColumnsBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        columns: [Column]
      }

      # A block to list a series of employees, can have a heading
      type EmployeeListBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        employees: [Employee]
      }

      # A block to list up to three buckets with a logo
      # LanguageStrings in Columns can include HTML
      type Bucket {
        heading: LanguageStrings
        body: LanguageStrings
        icon: String
      }

      type BucketBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        buckets: [Bucket]
      }

      # A block to represent a timeline
      # Milestones can include inline HTML
      type TimelineEntry {
        heading: LanguageStrings,
        milestones: [LanguageStrings]
      }

      type TimelineBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        entries: [TimelineEntry]
      }

      # A block for displaying an array of strings as a slider
      type CustomerSliderBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        backgroundImage: TaggedImage
        customers: [String]
      }

      # A block that shows the values / features of the company
      type ValuesBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
      }

      # A block that shows tabs with icons and optionally a separator
      type Tab {
        icon: String
        label: LanguageStrings
        columns: [LanguageStrings]
        separatorBackground: TaggedImage
        separatorHeading: LanguageStrings
        separatorText: LanguageStrings
        separatorCTA: Cta
      }

      type TabsBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        separator: Boolean
        tabs: [Tab]
      }

      # A block to represent contact data
      type ContactBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        address: String
        email: String
        phone: String
      }

      # A full-screen image slider with a content area in the centre
      type FullscreenSliderBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        subheading: LanguageStrings
        ctas: [Cta]
        images: [TaggedImage]
        scrollAnchor: String
      }

      # A block containing a list of link cards, optionally with heading and a paragraph

      type ServicesBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        text: LanguageStrings
        servicesTagged: String
      }

      # Different separator blocks
      type CtaSeparatorBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        text: LanguageStrings
        cta: Cta
        image: TaggedImage
        backgroundImage: TaggedImage
      }

      type LabelledIcon {
        icon: String
        label: LanguageStrings
      }

      type IconSeparatorBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        icons: [LabelledIcon],
        backgroundImage: TaggedImage,
      }

      # A block for introducing a page that doesn’t have a fullscreen slider
      type PageTitleBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        backgroundImage: TaggedImage
      }

      # A block for showing an accordeon of open positions next to an image
      type OpenPositionsBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        images: [TaggedImage]
      }

      # A block showing a list of all download items available for a language
      type DownloadItemsBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
      }

      # A block showing a list of specified download items available for a language
      type SpecificDownloadItemsBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        downloads: [DownloadItem]
      }

      # A block showing a series of images with an included lightbox
      type GalleryBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        images: [TaggedImage]
      }

      # A block for showing related pages
      type RecoveryBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
      }

      # A block showing a generic accordeon
      type AccordeonItem {
        heading: LanguageStrings
        body: LanguageStrings # may contain HTML
      }

      type AccordeonBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        items: [AccordeonItem]
        redAccent: Boolean
      }

      # A block for showing a single image with a mobile fallback as well as a title and some text
      type SingleImageBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        body: LanguageStrings # may contain inline HTML
        image: TaggedImage
        mobileImage: TaggedImage
        backgroundColor: String
        lightText: Boolean
      }

      # A block for showing numbered columns
      type NumberedCell {
        heading: LanguageStrings,
        body: LanguageStrings
      }

      type NumberedGridBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        cells: [NumberedCell]
        backgroundImage: TaggedImage
      }

      # A block for loading a flipbook pdf
      type FlipbookBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        flipbookURL: String
      }

      # A block for showing a paginated list of blog posts
      type BlogRollBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
      }

      # A block for showing the three latest blog posts related to an optional categories
      type RecentPostsBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        categories: [Category]
        backgroundColor: String
      }

      # A block showing a list of all download items available for a language
      type TeamGalleryBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
      }

      # A block for showing a custom form
      type FormBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        body: LanguageStrings # may contain HTML
        cta: Cta
        highlight: LanguageStrings # may contain inline HTML
        formId: String
        submitButtonLabel: LanguageStrings
      }

      # A block for loading a Gltf Model as well as show a title and some text
      type GltfBlock implements Block {
        template: String!
        id: String
        shortLinkLabel: LanguageStrings
        heading: LanguageStrings
        body: LanguageStrings # may contain inline HTML
        backgroundColor: String
        lightText: Boolean
        modelUrl: String
      }

      # Page meta type
      type PageMeta {
        canonicalUrl: LanguageStrings,
        description: LanguageStrings,
        image: TaggedImage,
        cardStyle: String,
        keywords: LanguageStringArray
      }

      # Main Page Type (imported from content/pages)
      type PageNode implements Node @infer {
        breadcrumb: [BreadcrumbSegment]
        id: ID!
        languages: [String]!
        template: String!
        templateOverrides: LanguageStrings
        name: LanguageStrings
        tags: [String]
        meta: PageMeta
        menuIndex: Int
        url: LanguageStrings
        content: [Block]
        relatedPages: [String] # the tag from which to fetch related pages
        color: String # a custom page color
      }

      # Blog Post Type (imported from content/blog)
      type BlogNode implements Node @infer {
        authors: [Employee] @reference(by: "id")
        categories: [Category] @reference(by: "id")
        date: Date
        heroImage: TaggedImage
        heroVideo: String
        id: ID!
        language: String
        name: String
        teaser: String
        contact: Employee
      }
    `);

    // This is needed so we can properly associate the Type of each block
    // See: https://stackoverflow.com/questions/52088172/how-do-you-handle-an-array-of-multiple-types-ex-different-content-blocks-in-g
    // It needs to be done like this since addSchemaResolvers can’t find a Block union/interface to add the custom resolver to
    // then associate the template (the name of the block-front-matter-template in Forestry)
    // with the correct type
    addSchemaTypes([
      schema.createInterfaceType({
        name: 'Block',
        fields: {
          template: 'String!',
          id: 'String',
          shortLinkLabel: 'LanguageStrings',
        },
        resolveType(obj) { // eslint-disable-line consistent-return
          if (obj.template === 'text') return 'TextBlock';
          if (obj.template === 'long-text') return 'LongformTextBlock';
          if (obj.template === 'columns') return 'ColumnsBlock';
          if (obj.template === 'employee-list') return 'EmployeeListBlock';
          if (obj.template === 'bucket') return 'BucketBlock';
          if (obj.template === 'timeline') return 'TimelineBlock';
          if (obj.template === 'customer-slider') return 'CustomerSliderBlock';
          if (obj.template === 'values') return 'ValuesBlock';
          if (obj.template === 'tabs') return 'TabsBlock';
          if (obj.template === 'contact') return 'ContactBlock';
          if (obj.template === 'fullscreen-slider') return 'FullscreenSliderBlock';
          if (obj.template === 'services') return 'ServicesBlock';
          if (obj.template === 'cta-separator') return 'CtaSeparatorBlock';
          if (obj.template === 'icon-separator') return 'IconSeparatorBlock';
          if (obj.template === 'page-title') return 'PageTitleBlock';
          if (obj.template === 'open-positions') return 'OpenPositionsBlock';
          if (obj.template === 'download-items') return 'DownloadItemsBlock';
          if (obj.template === 'specific-download-items') return 'SpecificDownloadItemsBlock';
          if (obj.template === 'gallery') return 'GalleryBlock';
          if (obj.template === 'recovery') return 'RecoveryBlock';
          if (obj.template === 'accordeon') return 'AccordeonBlock';
          if (obj.template === 'single-image') return 'SingleImageBlock';
          if (obj.template === 'numbered-grid') return 'NumberedGridBlock';
          if (obj.template === 'flipbook') return 'FlipbookBlock';
          if (obj.template === 'blog-roll') return 'BlogRollBlock';
          if (obj.template === 'recent-posts') return 'RecentPostsBlock';
          if (obj.template === 'team-gallery') return 'TeamGalleryBlock';
          if (obj.template === 'gltf') return 'GltfBlock';
          if (obj.template === 'form') return 'FormBlock';
        },
      }),
    ]);

    function localizedResolver(obj, args) {
      const wantedLanguage = obj[args.lang || 'de'];
      if (!wantedLanguage) return obj[fallbackLanguage];
      return wantedLanguage;
    }

    function allResolver(obj) {
      return JSON.stringify(obj);
    }

    addSchemaResolvers({
      LanguageStrings: {
        localizedString: {
          type: 'String',
          args: {
            lang: 'String',
          },
          resolve: localizedResolver,
        },
        allLanguages: { // a little bit hacky but since we need all the language in one type, this seems feasible
          type: 'String',
          resolve: allResolver,
        },
      },
      LanguageStringArray: {
        localizedString: {
          type: '[String]',
          args: {
            lang: 'String',
          },
          resolve: localizedResolver,
        },
        allLanguages: {
          type: 'String',
          resolve: allResolver,
        },
      },
    });

    const blogPosts = getCollection('BlogNode');
    blogPosts.data().forEach((post) => {
      if (!post.date) blogPosts.removeNode(post.id);
    });
  });

  api.onCreateNode((options) => {
    if (options.internal.typeName === 'PageNode') {
      const url = { ...options.url };
      options.languages.forEach((lang) => {
        if (!url[lang]) {
          let breadcrumbString = '';
          if (options.breadcrumb) {
            let breadcrumbPath = lang === 'de' ? '' : `/${lang}`;
            options.breadcrumb.forEach((part, index) => {
              breadcrumbString += `/${slugify(part.label[lang], { decamelize: false })}`;

              if (index > 0 && options.breadcrumb[index - 1].hash) [breadcrumbPath] = breadcrumbPath.split('/#');

              if (part.hash) breadcrumbPath += `/${part.hash}`;
              else breadcrumbPath += `/${lang === 'de' ? slugify(part.label[lang], { customReplacements: [['&', ' und ']], decamelize: false }) : slugify(part.label[lang], { decamelize: false })}`;

              if (!part.href) part.href = { [lang]: part.hash ? breadcrumbPath : `${breadcrumbPath}/` }; // eslint-disable-line no-param-reassign
              else part.href[lang] = part.hash ? breadcrumbPath : `${breadcrumbPath}/`; // eslint-disable-line no-param-reassign
            });
          }

          if (lang === 'de') url[lang] = `${breadcrumbString}/${slugify(options.name[lang], { customReplacements: [['&', ' und ']], decamelize: false })}/`;
          else url[lang] = `/${lang}${breadcrumbString}/${slugify(options.name[lang], { decamelize: false })}/`;
        }
      });
      return { ...options, url };
    }

    if (options.internal.typeName === 'Category') {
      const url = {};
      supportedLanguages.forEach((lang) => { // create a url for each category with a label in a supported language
        if (lang === 'de' && options.label.de) url[lang] = `/news/${slugify(categoryTranslated.de, { decamelize: false })}/${slugify(options.label.de, { decamelize: false })}/`;
        else if (options.label[lang]) url[lang] = `/${lang}/news/${slugify(categoryTranslated[lang], { decamelize: false })}/${slugify(options.label[lang], { decamelize: false })}/`;
      });

      return { ...options, url };
    }

    if (options.internal.typeName === 'OpenPosition') {
      const url = {};
      supportedLanguages.forEach((lang) => {
        if (lang === 'de' && options.name.de && options.body.de) url[lang] = `/${slugify(openPositionSlug.de, { decamelize: false })}/${slugify(options.name.de, { decamelize: false })}/`;
        else if (options.name[lang] && options.body[lang]) url[lang] = `/${lang}/${slugify(openPositionSlug[lang], { decamelize: false })}/${slugify(options.name[lang], { decamelize: false })}/`;
      });

      return { ...options, url };
    }

    return options;
  });

  api.createPages(async ({ createPage, graphql }) => {
    const allLangs = supportedLanguages.join(',\n');
    const { data } = await graphql(`{
      allPageNode {
        edges {
          node {
            id,
            content {
              template
              ...on RecentPostsBlock {
                categories {
                  id
                }
              }
            }
            languages,
            relatedPages,
            template,
            templateOverrides {
              ${allLangs}
            },
            url {
              ${allLangs}
            }
          }
        }
      }
      allBlogNode {
        edges {
          node {
            id
            language
            path
          }
        }
      }
      allCategory {
        edges {
          node {
            id
            url {
              ${allLangs}
            }
          }
        }
      }
      allOpenPosition {
        edges {
          node {
            id
            url {
              ${allLangs}
            }
          }
        }
      }
    }`);

    data.allPageNode.edges.forEach(({ node }) => {
      const blogRollIndex = node.content.findIndex((block) => block.template === 'blog-roll');
      const recentPostsIndex = node.content.findIndex((block) => block.template === 'recent-posts');
      const hasBlogroll = blogRollIndex !== -1;
      const hasRecentPosts = recentPostsIndex !== -1;
      const recentBlogNodesCategories = hasRecentPosts ? node.content[recentPostsIndex].categories.map((category) => category.id) : null;

      node.languages.forEach((lang) => {
        if (supportedLanguages.includes(lang)) {
          const page = {
            context: {
              id: node.id,
              lang,
              relatedPages: node.relatedPages,
              queryBlogNodes: hasBlogroll,
              queryRecentBlogNodes: hasRecentPosts,
              recentBlogNodesCategories,
            },
            path: node.url[lang],
          };

          if (node.templateOverrides && node.templateOverrides[lang]) page.component = `./src/templates/${node.templateOverrides[lang]}.vue`;
          else page.component = `./src/templates/${node.template}.vue`;

          createPage(page);
        }
      });
    });

    data.allBlogNode.edges.forEach(({ node }) => {
      const page = {
        context: {
          id: node.id,
          lang: node.language,
        },
        path: node.path,
        component: './src/templates/BlogPost.vue',
      };

      createPage(page);
    });

    data.allCategory.edges.forEach(({ node }) => {
      if (node.url) {
        Object.keys(node.url).forEach((lang) => {
          if (node.url[lang]) {
            const page = {
              context: {
                id: node.id,
                lang,
              },
              path: node.url[lang],
              component: './src/templates/BlogCategory.vue',
            };

            createPage(page);
          }
        });
      }
    });

    data.allOpenPosition.edges.forEach(({ node }) => {
      if (node.url) {
        Object.keys(node.url).forEach((lang) => {
          if (node.url[lang]) {
            const page = {
              context: {
                id: node.id,
                lang,
              },
              path: node.url[lang],
              component: './src/templates/JobListing.vue',
            };

            createPage(page);
          }
        });
      }
    });

    // Add the preview route manually so we can preserve leading underscores
    createPage({
      path: '/___preview/',
      component: './src/templates/Preview.vue',
    });
  });
};
